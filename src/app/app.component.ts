import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LazyLoadEvent } from 'primeng/api';
import { ApiService } from './api.service';
import { loadPage } from './store/app.action';
import { all } from './store/app.selector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  data: any = {}
  first = 0;
  rows = 5;

  constructor(private apiService: ApiService, private store: Store) { }

  loadData(event: LazyLoadEvent) {
    const start = event.first ?? 0
    const end = start + this.rows

    //timeout prevent error  Expression has changed after it was checked
    setTimeout(() => {
      this.store.select(all(start, end)).subscribe(dataStore => {
        if (dataStore.length < 1) {
          this.apiService.getData(start, end).subscribe(result => {
            console.log('>>>> not found in ngrx, getting data from BE')
            this.data.count = result.count
            this.store.dispatch(loadPage({
              start, end,
              data: result.data
            }))
          })
        } else {
          console.log('>>>> found data in ngrx, restoring data from ngrx')
          this.data = { ...this.data, data: dataStore }
        }
      })
    }, 100)
  }
}
