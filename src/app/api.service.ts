import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

@Injectable({
    providedIn : 'root'
})
export class ApiService {

    getData(start? : number, limit? : number) : Observable<any> {
        const d = { data : dummyData.slice(start, limit), count : dummyData.length }
        return of(d)
    }

}

const dummyData = [
    1, 2, 3, 4, 5, 
    6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 
    16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 
    26, 27, 28, 29, 30,
    31, 32, 33, 34, 35, 
    36, 37, 38, 39, 40
]