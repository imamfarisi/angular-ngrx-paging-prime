import { createFeatureSelector, createSelector } from "@ngrx/store";
import { State } from "./app.reducer";

export const all = (start?: number, end?: number) => createSelector(
    createFeatureSelector('page'),
    (state: State) => {
        const filterDate = state.data.filter(d => d.start == start && d.end == end)
        return filterDate.length < 1 ? [] : filterDate[0].data
    }
);