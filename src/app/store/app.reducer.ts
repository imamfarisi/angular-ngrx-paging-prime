import { createReducer, on } from "@ngrx/store";
import { loadPage } from "./app.action";

export interface State {
    data: {
        start: number, end: number,
        data: any[]
    }[]
}

export const initialState: State = {
    data: [],
};

export const pageReducer = createReducer(
    initialState,
    on(loadPage, (state, payload) => {
        const newState = {
            data: [...state.data, { data: payload.data, start: payload.start, end: payload.end }]
        }
        return newState
    }),
);