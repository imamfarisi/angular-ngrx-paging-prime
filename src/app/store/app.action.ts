import { createAction, props } from '@ngrx/store';

export const loadPage = createAction(
    'Load Page',
    props<{
        data: any[],
        start: number, end: number
    }>()
);